import { useSelector } from "react-redux";
import { TodoGroup } from "../components/TodoGroup/TodoGroup";
const DoneList = () => {
    const todos = useSelector((state) => state.todoList.todoList);
    return ( 
        <div>
            <h1>Todo List</h1>
            <TodoGroup todoList={todos.filter((todo) => todo.done)} canModify={false}/>
        </div>
     );
}
 
export default DoneList;