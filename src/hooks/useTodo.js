import { useDispatch } from "react-redux"
import { findTodoById, loadTodos, deleteTodo, updateTodo, addNewTodo } from "../apis/todo"
import { updateList } from "../components/todoListSlice";
import { useRef } from "react";
import { message } from "antd";
export const useTodo = () => {
    const dispatch = useDispatch();
    const reloadTodos = () => {
        loadTodos()
        .then(res => {
            dispatch(updateList(res.data));
        })
        .catch(err => {
            dispatch(updateList([]));
            message.error("Request failed")
        })
    };

    const loadTodoById = (id) => {
        return findTodoById(id);
    };

    const deleteTodoById = async (id) => {
        await deleteTodo(id);
        await reloadTodos();
    }

    const updateTodoById = async (id, todo) => {
        await updateTodo(id, todo);
        await reloadTodos();
    }

    const addTodo = async (text) => {
        await addNewTodo({
            text,
            done: false
        })
        await reloadTodos();
    }

    return useRef(
        { reloadTodos, loadTodoById, deleteTodoById, updateTodoById, addTodo }
    ).current
}