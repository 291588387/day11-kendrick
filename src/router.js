import { TodoList } from './components/TodoList/TodoList';
import { createBrowserRouter } from "react-router-dom";
import AboutPage from './pages/AboutPage';
import DoneList from './pages/DoneList';
import Layout from './components/Layout';
import TodoDetail from './components/TodoList/TodoDetail';
import NotFoundPAge from './pages/NotFoundPage';

export const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <Layout />
    ),
    children: [
      {
        path: '/',
        element: <TodoList />
      },
      {
        path: '/done',
        element: <DoneList />
      },{
        path: '/about',
        element: <AboutPage />
      },
      {
        path: 'todo/:id',
        element: <TodoDetail />
      },
      {
        path: '*',
        element: <NotFoundPAge />
      }
    ]
  },
]);
