import request from "./request";

export const loadTodos = () => {
    return request.get();
}

export const findTodoById = (id) => {
    return request.get(`/${id}`);
}

export const addNewTodo = (todo) => {
    return request.post('', todo);
}

export const deleteTodo = (id) => {
    return request.delete(`/${id}`,);
}

export const updateTodo = (id, todo) => {
    return request.put(`/${id}`, todo);
}

