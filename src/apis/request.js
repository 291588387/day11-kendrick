import axios from "axios";
import { message } from "antd";
const request = axios.create({
    baseURL: 'http://localhost:8081/todos'
})

request.interceptors.response.use(
    (response) => response,
    (error) => {
        const msg = error.response.data?.message;
        if (msg) {
            message.error(msg);
        }
        return Promise.reject(error);
    }
)

export default request