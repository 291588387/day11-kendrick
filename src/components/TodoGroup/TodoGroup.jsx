import './TodoGroup.css';
import { TodoItem } from '../TodoItem/TodoItem';

export const TodoGroup = (props) => {
    const todoItems = props.todoList.map((item) => {
        return <TodoItem todo={item} key={item.id} canModify={props.canModify}/>
    });
    return (
        <div className="todoGroup">
            {todoItems}
        </div>
    )
}