import './TodoList.css';
import { TodoGroup } from '../TodoGroup/TodoGroup';
import { TodoGenerator } from '../TodoGenerator/TodoGenerator';
import { useSelector } from 'react-redux';
import { useEffect } from 'react';
import { useTodo } from '../../hooks/useTodo';

export const TodoList = () => {
    const { reloadTodos } = useTodo();
    useEffect(() => {
        reloadTodos();
    }, [reloadTodos]);
    const todoList = useSelector((state) => state.todoList.todoList);
    return (
        <div className="todoList">
            <h1>Todo List</h1>
            <TodoGroup todoList={todoList} canModify={true} />
            <TodoGenerator />
        </div>
    )
}