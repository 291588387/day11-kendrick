import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useTodo } from '../../hooks/useTodo';

const TodoDetail = () => {
    const { loadTodoById } = useTodo();
    const {id} = useParams();
    const [targetTodo, setTargetTodo] = useState({});
    const navigate = useNavigate();

    useEffect(() => {
        loadTodoById(id) 
        .then((res) => {
            setTargetTodo(res.data);
        })
        .catch(() => {
            navigate('/404')
        })
    }, []);
    return ( 
        <>
        <h1>Detail</h1>
            {targetTodo && <p>{targetTodo.text}</p>}
        </>
     );
}
 
export default TodoDetail;