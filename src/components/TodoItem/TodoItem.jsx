import './TodoItem.css';
import { useNavigate } from 'react-router-dom';
import { useTodo } from '../../hooks/useTodo';
import Card from 'antd/es/card/Card';
import { ExclamationOutlined, CloseOutlined, CheckOutlined, EditOutlined } from '@ant-design/icons'
import { Button, Modal, Input, message } from 'antd';
import { useState } from 'react';
export const TodoItem = (props) => {
    const [modalVisible, setModalVisible] = useState(false);
    const [todoText, setTodoText] = useState(props.todo.text);
    const navigate = useNavigate();
    const { updateTodoById, deleteTodoById } = useTodo();
    const toggleDone = () => {
        if (props.canModify) {
            let { id, text, done } = props.todo;
            updateTodoById(id, { id, text, done: !done });
        } else {
            navigate(`/todo/${props.todo.id}`);
        }
    }
    const handleDelete = (event) => {
        event.stopPropagation();
        deleteTodoById(props.todo.id);
        message.success('Delete Successfully!')
    }

    const handleOpenModal = (event) => { 
        setTodoText(props.todo.text);
        event.stopPropagation();
        setModalVisible(true); 
    }

    const handleCancel = () => { 
        setModalVisible(false); 
        setTodoText('');
    }

    const handleOk = () => {
        if (!todoText.trim()) {
            message.error('Please input');
            return;
        }
        const { id, done } = props.todo;
        updateTodoById(id, { id, text: todoText, done })
        .then(() => {
            setModalVisible(false);
            message.success('Edit Successfully!')
        });
    }

    return (
        <>
            <Card
                className='card'
                title={props.todo.done ? <CheckOutlined style={{"color": 'green'}} /> : <ExclamationOutlined style={{"color": 'rgb(238, 178, 12)'}} />}
                size='small'
                onClick={toggleDone}
            >
                <div className="todoItem">
                    <span className={props.todo.done ? 'doneText' : 'text'}>{props.todo.text}</span>
                    {props.canModify ?
                        <>
                            <div className="buttonArea">
                                <Button className='icon-botton' shape="circle" icon={<EditOutlined />} onClick={handleOpenModal} size='small' />
                                <Button className='icon-botton' danger shape="circle" icon={<CloseOutlined />} onClick={handleDelete} size='small' />
                            </div>
                        </> : <></>}
                </div>
            </Card>
            <Modal title="Edit todo" open={modalVisible} onOk={handleOk} onCancel={handleCancel} centered={true}>
                <Input value={todoText} onChange={(event) => {setTodoText(event.target.value);}} />
            </Modal>
        </>
    )
}