import { createSlice } from '@reduxjs/toolkit'

export const todoListSlice = createSlice({
  name: "todoList",
  initialState: {
    todoList: [],
  },
  reducers: {
    updateList: (state, action) => {
      state.todoList = action.payload;
    }
  }
});

// Action creators are generated for each case reducer function
export const { updateList } = todoListSlice.actions;

export default todoListSlice.reducer;