import './TodoGenerator.css';
import { useState } from 'react';
import { useTodo } from '../../hooks/useTodo';
import { Button, Input, message } from 'antd';
export const TodoGenerator = () => {
    const [inputTodo, setInputTodo] = useState('');
    const { addTodo } = useTodo();
    const handleAddTodo = () => {
        if (inputTodo.trim() === '') {
            alert("Please Input");
            return;
        }
        addTodo(inputTodo)
        .then(() => {
            message.success('Add successfully!')
            setInputTodo('');
        })
        .catch(err => {});
    }
    const handleKeyUp = (event) => {
        if (event.keyCode === 13) handleAddTodo();
    }

    return (
        <div className="todoGenerator">
            <Input
                placeholder='Please input new todo'
                className="input"
                value={inputTodo}
                onKeyUp={handleKeyUp}
                onChange={(event) => setInputTodo(event.target.value)} />
            <Button type="primary" className="button" onClick={handleAddTodo}>add</Button>
        </div>
    )
}