## O:

- Introduced React Router to apply in multiple routes application.
- Practice react router to apply it in todoList demo. Got the hang of basic usage of react router and frequently used components
- Learned about how to integrate frontend and backend together, by using Axios, a commonly used http request library.
- With helped of MockApi, I managed to practice how to replace redux operation with actual http requests to maintain todoList

## R:

- Refreshing

## I:

- Using react router is a experience with familiar ideology but some brand new operations.
- Procedure of migrating from redux to axios requests left me great impression on how to use axios and customize hooks properly.
- usage of useRef is quite confusing and takes time to digest.

## D:

- Analyze some outstanding React projects to figure out proper ways of using different hooks


