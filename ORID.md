## O:

- Distributed different task about Elevator Speech and MVP presentation in group after code review.
- Introduced Redux, a library to manage global state. Understood the UI -> Action -> Reducer -> Store data flow.
- Practiced redux by refactoring counter demo and todoList demo.
- Learned about JEST and how to run frontend test with it.

## R:

- Fruitful

## I:

- I was well benefited during procedure of transition from react native to react redux.
- ⁠It pretty confusing when using slicer component of Redux
- It doesn't seem too convenient to use Redux for state management

## D:

- Explore the details in one way flow of redux and figure out its commons and differences with VueX


